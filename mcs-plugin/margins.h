/*      $Id$

        This program is free software; you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation; either version 2, or (at your option)
        any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program; if not, write to the Free Software
        Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

        Copyright (C) 2002-2006 Jasper Huijsmans (huysmans@users.sourceforge.net)
                                Olivier Fourdan (fourdan@xfce.org)
*/

#ifndef INC_MARGINS_H
#define INC_MARGINS_H

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

void create_margins_channel (McsPlugin * mcs_plugin);

void add_margins_page (GtkBox *box);

#endif /* INC_MARGINS_H */

