# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: xfwm4 4.4.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2007-06-23 19:57+0100\n"
"PO-Revision-Date: 2006-10-26 20:28+0530\n"
"Last-Translator: sonam rinchen <somindruk@druknet.bt>\n"
"Language-Team: dzongkha <pgeyleg@dit.gov.bt>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2;plural=(n!=1)\n"
"X-Poedit-Language: dzongkha\n"
"X-Poedit-Country: bhutan\n"
"X-Poedit-SourceCharset: utf-8\n"

#: ../mcs-plugin/margins.c:130
msgid "Workspace Margins"
msgstr "ལཱ་གི་ས་སྒོའི་ས་སྟོང་ཚུ།"

#: ../mcs-plugin/margins.c:136
msgid ""
"Margins are areas on the edges of the screen where no window will be placed"
msgstr "ས་སྟོང་ཚུ་སྒོ་སྒྲིག་མི་བཞག་ནི་ཨིན་མིའི་གསལ་གཞི་གི་མཐའམ་ཚུ་གུའི་མངའ་ཁོངས་ཚུ་ཨིན།"

#: ../mcs-plugin/margins.c:167
msgid "Left :"
msgstr "གཡོན་:"

#: ../mcs-plugin/margins.c:187
msgid "Right :"
msgstr "གཡས་:"

#: ../mcs-plugin/margins.c:207
msgid "Top :"
msgstr "མགོ་:"

#: ../mcs-plugin/margins.c:227
msgid "Bottom :"
msgstr "མཇུག་:"

#. the button label in the xfce-mcs-manager dialog
#: ../mcs-plugin/workspaces_plugin.c:110
msgid "Button Label|Workspaces and Margins"
msgstr "ཨེབ་རྟའི་ཁ་ཡིག་|ལཱ་གི་ས་སྒོ་ཚུ་དང་ས་སྟོང་ཚུ།"

#: ../mcs-plugin/workspaces_plugin.c:144
msgid "Workspaces and Margins"
msgstr "ལཱ་གི་ས་སྒོ་ཚུ་དང་ས་སྟོང་ཚུ།"

#: ../mcs-plugin/workspaces_plugin.c:162 ../mcs-plugin/wmtweaks_plugin.c:640
#: ../mcs-plugin/workspaces.c:516
msgid "Workspaces"
msgstr "ལཱ་གི་ས་སྒོ་ཚུ།"

#: ../mcs-plugin/workspaces_plugin.c:171
msgid "Margins"
msgstr "ས་སྟོང་ཚུ།"

#: ../mcs-plugin/wmtweaks_plugin.c:471
msgid "None"
msgstr "ཅི་མེད།"

#: ../mcs-plugin/wmtweaks_plugin.c:476
#, fuzzy
msgid "Bring window on current workspace"
msgstr "ཤུལ་མམ་གྱི་ལཱ་གི་ས་སྒོ་ལུ་སྒོ་སྒྲིག་བཤུད།"

#: ../mcs-plugin/wmtweaks_plugin.c:477
#, fuzzy
msgid "Switch to window's workspace"
msgstr "ལཱ་གི་ས་སྒོ་ %d ལུ་སྒོ་སྒྲིག་བཤུད།"

#: ../mcs-plugin/wmtweaks_plugin.c:478
#, fuzzy
msgid "Do nothing"
msgstr "ག་ནི་ཡང་མེདཔ།"

#: ../mcs-plugin/wmtweaks_plugin.c:483
msgid "Place window under the mouse"
msgstr ""

#: ../mcs-plugin/wmtweaks_plugin.c:484
msgid "Place window in the center"
msgstr ""

#: ../mcs-plugin/wmtweaks_plugin.c:493
#: ../mcs-plugin/xfce-wmtweaks-settings.desktop.in.h:2
msgid "Window Manager Tweaks"
msgstr "སྒོ་སྒྲིག་འཛིན་སྐྱོང་པ་གཅུཝ་ཨིན།"

#: ../mcs-plugin/wmtweaks_plugin.c:512
msgid ""
"Skip windows that have \"skip pager\" or \"skip taskbar\" properties set"
msgstr "\"skip pager\" ཡང་ན་ \"skip taskbar\" རྒཡུ་དངོས་ཚུའི་ཆ་ཚན་ཡོད་མི་ཝིན་ཌོསི་གོམ་འགྱོ།"

#: ../mcs-plugin/wmtweaks_plugin.c:518
msgid "Include hidden (i.e. iconified) windows"
msgstr "གསང་བ་ (i.e. ངོས་དཔར་བཟོ་ཡོད་མི་) ཝིན་ཌོསི་གྲངས་སུ་བཙུགས།"

#: ../mcs-plugin/wmtweaks_plugin.c:524
msgid "Cycle through windows from all workspaces"
msgstr "ལཱ་གི་ས་སྒོ་ཚུ་ཆ་མཉམ་ལས་སྒོ་སྒྲིག་ཚུ་བརྒྱུད་དེ་བསྐྱར་འཁོར།"

#: ../mcs-plugin/wmtweaks_plugin.c:529
msgid "Cycling"
msgstr "བསྐྱར་འཁོར་འབད་དོ།"

#: ../mcs-plugin/wmtweaks_plugin.c:541
msgid "Activate focus stealing prevention"
msgstr "ཆེད་དམིགས་ཨར་རྐུན་བཀག་ཐབས་ཤུགས་ལྡན་བཟོ།"

#: ../mcs-plugin/wmtweaks_plugin.c:547
msgid "Honor the standard ICCCM focus hint"
msgstr "ཚད་ལྡན་ཨའི་སི་སི་སི་ཨེམ་ཆེད་དམིགས་བརྡ་མཚོན་བརྩི་འཇོག་འབད།"

#: ../mcs-plugin/wmtweaks_plugin.c:553
msgid "When a window raises itself:"
msgstr ""

#: ../mcs-plugin/wmtweaks_plugin.c:558 ../mcs-plugin/xfwm4_plugin.c:1752
msgid "Focus"
msgstr "ཆེད་དམིགས།"

#: ../mcs-plugin/wmtweaks_plugin.c:570
msgid "Key used to grab and move windows"
msgstr "ཝིན་ཌོསི་འཛིན་ནི་དང་བཤུད་ནི་ལུ་ལག་ལེན་འཐབ་མི་ ལྡེ་མིག།"

#: ../mcs-plugin/wmtweaks_plugin.c:576
msgid "Raise windows when any mouse button is pressed"
msgstr "མཱའུསི་ཨེབ་རྟ་གང་རུང་ཅིག་ཨེབ་པའི་སྐབས་ཝིན་ཌོསི་ཆེར་བསྐྱེད་འབད།"

#: ../mcs-plugin/wmtweaks_plugin.c:583
msgid "Hide frame of windows when maximized"
msgstr "སྦོམ་བཟོ་བའི་སྐབས་ཝིན་ཌོསི་གི་གཞི་ཁྲམ་སྦ་བཞག།"

#: ../mcs-plugin/wmtweaks_plugin.c:590
msgid "Restore original size of maximized windows when moving"
msgstr "སྤོ་བཤུད་འབད་བའི་སྐབས་སྦོམ་བཟོ་ཡོད་མི་ཝིན་ཌོསི་གི་ཚད་ངོ་མ་སོར་ཆུད་འབད།"

#: ../mcs-plugin/wmtweaks_plugin.c:596
msgid "Use edge resistance instead of windows snapping"
msgstr "ཝིན་ཌོསི་པར་བཏབ་ནིའི་ཚབ་ལུ་བཀག་ནུས་ཀྱི་མཐའམ་ལག་ལེན་འཐབ།"

#: ../mcs-plugin/wmtweaks_plugin.c:601
msgid "Accessibility"
msgstr "འཛུལ་སྤྱོད།"

#: ../mcs-plugin/wmtweaks_plugin.c:614
msgid "Switch workspaces using the mouse wheel over the desktop"
msgstr "ཌེཀསི་ཊོཔ་གི་ལྟག་ལས་མཱའུསི་འཁོར་ལོ་ལག་ལེན་འཐབ་ཐོག་ལས་ལཱ་གི་ས་སྒོ་ཚུ་སོར་བསྒྱུར་འབད།"

#: ../mcs-plugin/wmtweaks_plugin.c:621
msgid ""
"Remember and recall previous workspace when switching via keyboard shortcuts"
msgstr ""
"ལྡེ་སྒྲོམ་མགྱོགས་ཐབས་ཚུ་བརྒྱུད་དེ་སོར་བསྒྱུར་འབད་བའི་སྐབས་ཧེ་མའི་ལཱ་གི་ས་སྒོ་སླར་དྲན་ཡང་ན་སེམས་ཁར་བཞག།"

#: ../mcs-plugin/wmtweaks_plugin.c:628
msgid "Wrap workspaces depending on the actual desktop layout"
msgstr "ཌེཀསི་ཊོཔ་སྒྲིག་བཀོད་ངོ་མ་ལུ་བརྟེན་ཏེ་ལཱ་གི་ས་སྒོ་ཚུ་ལོག་མཚམས་བཟོ།"

#: ../mcs-plugin/wmtweaks_plugin.c:635
msgid "Wrap workspaces when the first or last workspace is reached"
msgstr "དང་པམ་ཡང་ན་མཇུག་གི་ལཱ་གི་ས་སྒོ་ཚུ་ལྷོད་པའི་སྐབས་ལཱ་གི་ས་སྒོ་ཚུ་ལོག་མཚམས་བཟོ།"

#: ../mcs-plugin/wmtweaks_plugin.c:652
msgid "Minimum size of windows to trigger smart placement"
msgstr "སྤུར་བསྒྱིར་དྲགས་བཙུགས་བཞག་འབྱུང་སག་ས་འབད་ནི་ལུ་ཝིན་ཌོསི་གི་ཉུང་མཐའི་ཚད།"

#: ../mcs-plugin/wmtweaks_plugin.c:653
msgid "Size|Small"
msgstr "ཚད་|ཆུང་ཀུ།"

#: ../mcs-plugin/wmtweaks_plugin.c:653
msgid "Size|Large"
msgstr "ཚད་|སྦོམ།"

#: ../mcs-plugin/wmtweaks_plugin.c:658
#, fuzzy
msgid "Default positionning of windows without smart placement:"
msgstr "སྤུར་བསྒྱིར་དྲགས་བཙུགས་བཞག་འབྱུང་སག་ས་འབད་ནི་ལུ་ཝིན་ཌོསི་གི་ཉུང་མཐའི་ཚད།"

#: ../mcs-plugin/wmtweaks_plugin.c:663
msgid "Placement"
msgstr "བཙུགས་བཞག།"

#: ../mcs-plugin/wmtweaks_plugin.c:683
msgid "Enable display compositing"
msgstr "བཀྲམ་སྟོན་རྩོམ་བྲིས་འབད་ནི་ལྕོགས་ཅན་བཟོ།"

#: ../mcs-plugin/wmtweaks_plugin.c:697
msgid "Display full screen overlay windows directly"
msgstr "གསལ་གཞི་གངམ་སྤུར་ཁེབས་ཝིན་ཌོསི་ཐད་ཀར་དུ་བཀྲམ་སྟོན་འབད།"

#: ../mcs-plugin/wmtweaks_plugin.c:703
#, fuzzy
msgid "Show shadows under dock windows"
msgstr "པོཔ་ཨཔ་ཝིན་ཌོསི་གི་འོག་ལུ་གྱིབ་མ་ཚུ་སྟོན།"

#: ../mcs-plugin/wmtweaks_plugin.c:709
msgid "Show shadows under regular windows"
msgstr "དུས་རྒཡུན་ཝིན་ཌོསི་གི་འོག་ལུ་གྱིབ་མ་ཚུ་སྟོན།"

#: ../mcs-plugin/wmtweaks_plugin.c:715
msgid "Show shadows under popup windows"
msgstr "པོཔ་ཨཔ་ཝིན་ཌོསི་གི་འོག་ལུ་གྱིབ་མ་ཚུ་སྟོན།"

#: ../mcs-plugin/wmtweaks_plugin.c:721
msgid "Opacity of window decorations"
msgstr "སྒོ་སྒྲིག་མཛེས་བཀོད་ཚུ་གི་དྭངས་སྒྲིབ་"

#: ../mcs-plugin/wmtweaks_plugin.c:721 ../mcs-plugin/wmtweaks_plugin.c:727
#: ../mcs-plugin/wmtweaks_plugin.c:733 ../mcs-plugin/wmtweaks_plugin.c:739
#: ../mcs-plugin/wmtweaks_plugin.c:745
msgid "Transparent"
msgstr "དྭངས་གསལ་ཅན།"

#: ../mcs-plugin/wmtweaks_plugin.c:722 ../mcs-plugin/wmtweaks_plugin.c:728
#: ../mcs-plugin/wmtweaks_plugin.c:734 ../mcs-plugin/wmtweaks_plugin.c:740
#: ../mcs-plugin/wmtweaks_plugin.c:746
msgid "Opaque"
msgstr "དྭངས་སྒྲིབ་ཅན།"

#: ../mcs-plugin/wmtweaks_plugin.c:727
msgid "Opacity of inactive windows"
msgstr "ཤུགས་མེད་ཝིན་ཌོསི་གི་དྭངས་སྒྲིབ།"

#: ../mcs-plugin/wmtweaks_plugin.c:733
msgid "Opacity of windows during move"
msgstr "ཝིན་ཌོསི་སྤོ་བཤུད་འབད་བའི་སྐབས་ཀྱི་དྭངས་སྒྲིབ།"

#: ../mcs-plugin/wmtweaks_plugin.c:739
msgid "Opacity of windows during resize"
msgstr "ཝིན་ཌོསི་ཚད་བསྐྱར་བཟོ་འབད་བའི་སྐབས་ཀྱི་དྭངས་སྒྲིབ།"

#: ../mcs-plugin/wmtweaks_plugin.c:745
msgid "Opacity of popup windows"
msgstr "པོཔ་ཨཔ་ཝིན་ཌོསི་གི་དྭངས་སྒྲིབ།"

#: ../mcs-plugin/wmtweaks_plugin.c:750
msgid "Compositor"
msgstr "རྩོམ་བྲིས་པ།"

#. the button label in the xfce-mcs-manager dialog
#: ../mcs-plugin/wmtweaks_plugin.c:792
msgid "Button Label|Window Manager Tweaks"
msgstr "ཨེབ་རྟའི་ཁ་ཡིག་|སྒོ་སྒྲིག་འཛིན་སྐྱོང་པ་གཅུཝ་ཨིན།"

#: ../mcs-plugin/wmtweaks_plugin.c:914 ../mcs-plugin/xfwm4_plugin.c:2430
#, c-format
msgid "These settings cannot work with your current window manager (%s)"
msgstr "སྒྲིག་སྟངས་འདི་ཚུ་ཁྱོད་ཀྱི་ད་ལྟོའི་སྒོ་སྒྲིག་འཛིན་སྐྱོང་པ་ (%s)གཅིག་ཁར་ལཱ་འབད་མི་བཏུབ།"

#: ../mcs-plugin/workspaces.c:329
msgid "Change name"
msgstr "མིང་བསྒྱུར་བཅོས་འབད།"

#: ../mcs-plugin/workspaces.c:338 ../mcs-plugin/xfwm4_shortcuteditor.c:490
#, c-format
msgid "Workspace %d"
msgstr "ལཱ་གི་ས་སྒོ་ %d"

#: ../mcs-plugin/workspaces.c:348
msgid "Name:"
msgstr "མིང་:"

#: ../mcs-plugin/workspaces.c:427
msgid "Click on a workspace name to edit it"
msgstr "ལཱགི་ས་སྒོའི་མིང་ཞུན་དག་བརྐྱབ་ནི་ལུ་ལཱ་གི་ས་སྒོའི་མིང་གུ་ཨེབ་གཏང་།"

#: ../mcs-plugin/workspaces.c:493
msgid "Number of workspaces:"
msgstr "ལཱ་གི་ས་སྒོ་ཚུའི་གྲངས་:"

#: ../mcs-plugin/workspaces.c:527
msgid "Workspace names"
msgstr "ལཱ་གི་ས་སྒོའི་མིང་ཚུ།"

#: ../mcs-plugin/xfwm4_plugin.c:68
msgid "Menu"
msgstr "དཀར་ཆག།"

#: ../mcs-plugin/xfwm4_plugin.c:69
msgid "Stick"
msgstr "སྦྱར་ནི།"

#: ../mcs-plugin/xfwm4_plugin.c:70
msgid "Shade"
msgstr "ནག་གྲིབ།"

#: ../mcs-plugin/xfwm4_plugin.c:71
msgid "Hide"
msgstr "སྦ།"

#: ../mcs-plugin/xfwm4_plugin.c:72
msgid "Maximize"
msgstr "སྦོམ་བཟོ་ནི།"

#: ../mcs-plugin/xfwm4_plugin.c:73
msgid "Close"
msgstr "ཁ་བསྡམས།"

#: ../mcs-plugin/xfwm4_plugin.c:79 ../mcs-plugin/xfwm4_shortcuteditor.c:371
msgid "Shade window"
msgstr "ནག་གྲིབ་སྒོ་སྒྲིག།"

#: ../mcs-plugin/xfwm4_plugin.c:80 ../mcs-plugin/xfwm4_shortcuteditor.c:370
msgid "Hide window"
msgstr "སྒོ་སྒྲིག་སྦ་བཞག།"

#: ../mcs-plugin/xfwm4_plugin.c:81 ../mcs-plugin/xfwm4_shortcuteditor.c:364
msgid "Maximize window"
msgstr "སྒོ་སྒྲིག་སྦོམ་བཟོ།"

#: ../mcs-plugin/xfwm4_plugin.c:82 ../mcs-plugin/xfwm4_shortcuteditor.c:369
#, fuzzy
msgid "Fill window"
msgstr "སྒོ་སྒྲིག་སྦ་བཞག།"

#: ../mcs-plugin/xfwm4_plugin.c:83
msgid "Nothing"
msgstr "ག་ནི་ཡང་མེདཔ།"

#: ../mcs-plugin/xfwm4_plugin.c:88
msgid "Left"
msgstr "གཡོན།"

#: ../mcs-plugin/xfwm4_plugin.c:89
msgid "Center"
msgstr "དབུས།"

#: ../mcs-plugin/xfwm4_plugin.c:90
msgid "Right"
msgstr "གཡས།"

#: ../mcs-plugin/xfwm4_plugin.c:589
msgid "Click and drag buttons to change the layout"
msgstr "སྒྲིག་བཀོད་བསྒྱུར་བཅོས་འབད་ནི་ལུ་ཨེབ་གཏང་ཞིནམ་དང་ཨེབ་རྟ་ཚུ་འདྲུད།"

#: ../mcs-plugin/xfwm4_plugin.c:594
msgid "Active"
msgstr "ཤུགས་ལྡན།"

#: ../mcs-plugin/xfwm4_plugin.c:605
msgid "Title"
msgstr "མགོ་མིང་།"

#: ../mcs-plugin/xfwm4_plugin.c:606
msgid "The window title, it cannot be removed"
msgstr "སྒོ་སྒྲིག་མགོ་མིང་ དེ་རྩ་བསྐྲད་གཏང་མི་ཚུགས།"

#: ../mcs-plugin/xfwm4_plugin.c:618
msgid "Hidden"
msgstr "གསང་བ།"

#: ../mcs-plugin/xfwm4_plugin.c:1391
msgid "Font Selection Dialog"
msgstr "ཡིག་གཟུགས་སེལ་འཐུའི་ཌའི་ལོག།"

#: ../mcs-plugin/xfwm4_plugin.c:1456
msgid "Window Manager"
msgstr "སྒོ་སྒྲིག་འཛིན་སྐྱོང་པ།"

#: ../mcs-plugin/xfwm4_plugin.c:1496
msgid "Title font"
msgstr "མགོ་མིང་ཡིག་གཟུགས།"

#: ../mcs-plugin/xfwm4_plugin.c:1505
msgid "Title Alignment"
msgstr "མགོ་མིང་ཕྲང་སྒྲིག།"

#. XXX
#: ../mcs-plugin/xfwm4_plugin.c:1507
msgid "Text alignment inside title bar :"
msgstr "མགོ་མིང་ཕྲ་རིང་ནང་ལུ་ཚིག་ཡིག་ཕརང་སྒྲིག་:"

#: ../mcs-plugin/xfwm4_plugin.c:1513
msgid "Button layout"
msgstr "ཨེབ་རྟ་ཤཝསྒྲིག་བཀོད།"

#: ../mcs-plugin/xfwm4_plugin.c:1518
msgid "Style"
msgstr "བཟོ་རྣམ།"

#: ../mcs-plugin/xfwm4_plugin.c:1566
msgid "Window shortcuts"
msgstr "སྒོ་སྒྲིག་མགྱོགས་ཐབས་ཚུ།"

#: ../mcs-plugin/xfwm4_plugin.c:1580
msgid "Command"
msgstr "བརྡ་བཀོད།"

#: ../mcs-plugin/xfwm4_plugin.c:1586
msgid "Shortcut"
msgstr "མགྱོགས་ཐབས།"

#: ../mcs-plugin/xfwm4_plugin.c:1603
msgid "Keyboard"
msgstr "ལྡེ་སྒྲོམ།"

#: ../mcs-plugin/xfwm4_plugin.c:1618
msgid "Focus model"
msgstr "ཆེད་དམིགས་ཀྱི་དཔེ།"

#: ../mcs-plugin/xfwm4_plugin.c:1622
msgid "Click to focus"
msgstr "ཆེད་དམིགས་ཚུ་ལུ་ཨེབ་གཏང་།"

#: ../mcs-plugin/xfwm4_plugin.c:1632
msgid "Focus follows mouse"
msgstr "ཆེད་དམིགས་གིས་མཱའུསི་རྗེས་སུ་འབྲངམ་ཨིན།"

#: ../mcs-plugin/xfwm4_plugin.c:1645
#, fuzzy
msgid "Delay before window receives focus"
msgstr "ཆེད་དམིགས་འབད་ཡོད་པའི་སྒོ་སྒྲིག་ཆེར་བསྐྱེད་མ་འབད་བའི་ཧེ་མ་ཕྱིར་འགྱངས་འབད་:"

#: ../mcs-plugin/xfwm4_plugin.c:1654 ../mcs-plugin/xfwm4_plugin.c:1716
msgid "Slow"
msgstr "ལྷོད་ཆ།"

#: ../mcs-plugin/xfwm4_plugin.c:1661 ../mcs-plugin/xfwm4_plugin.c:1723
msgid "Fast"
msgstr "མགྱོགས་དྲགས།"

#: ../mcs-plugin/xfwm4_plugin.c:1682
msgid "Automatically give focus to newly created windows"
msgstr "གསརཔ་སྦེ་གསར་བསྐྲུན་འབད་ཡོད་པའི་ཝིན་ཌོསི་ལུ་རང་བཞིན་གྱིས་ཆེད་དམིགས་བྱིན།"

#: ../mcs-plugin/xfwm4_plugin.c:1686
msgid "New window focus"
msgstr "སྒོ་སྒྲིག་ཆེད་དམིགས་གསརཔ་ཚུ།"

#: ../mcs-plugin/xfwm4_plugin.c:1693
msgid "Raise on focus"
msgstr "ཆེད་དམིགས་གུ་ཆེར་བསྐྱེད་འབད།"

#: ../mcs-plugin/xfwm4_plugin.c:1699
msgid "Automatically raise windows when they receive focus"
msgstr "ཝིན་ཌོསི་གིས་ཆེད་དམིགས་འཐོབ་པའི་སྐབས་རང་བཞིན་གྱིས་ཝིན་ཌོསི་ཆེད་དམིགས་འབད།"

#: ../mcs-plugin/xfwm4_plugin.c:1709
msgid "Delay before raising focused window :"
msgstr "ཆེད་དམིགས་འབད་ཡོད་པའི་སྒོ་སྒྲིག་ཆེར་བསྐྱེད་མ་འབད་བའི་ཧེ་མ་ཕྱིར་འགྱངས་འབད་:"

#: ../mcs-plugin/xfwm4_plugin.c:1744
msgid "Raise window when clicking inside application window"
msgstr "གློག་རིམ་སྒོ་སྒྲིག་ནང་ལུ་ཨེབ་གཏང་པའི་སྐབས་སྒོ་སྒྲིག་ཆེར་བསྐྱེད་འབད།"

#: ../mcs-plugin/xfwm4_plugin.c:1748
msgid "Raise on click"
msgstr "ཨེབ་གཏང་གུ་ཆེར་བསྐྱེད་འབད།"

#: ../mcs-plugin/xfwm4_plugin.c:1766
msgid "Windows snapping"
msgstr "ཝིན་ཌོསི་པར་བཏབ་དོ།"

#: ../mcs-plugin/xfwm4_plugin.c:1771
msgid "Snap windows to screen border"
msgstr "གསལ་གཞིའི་མཐའ་མཚམས་ལུ་ཝིན་ཌོསི་པར་བཏབ།"

#: ../mcs-plugin/xfwm4_plugin.c:1777
msgid "Snap windows to other windows"
msgstr "གཞན་ཝིན་ཌོསི་ལུ་ཝིན་ཌོསི་པར་བཏབ།"

#: ../mcs-plugin/xfwm4_plugin.c:1788
msgid "Distance :"
msgstr "གྱང་ཚད་:"

#: ../mcs-plugin/xfwm4_plugin.c:1795
msgid "Distance|Small"
msgstr "གྱང་ཚད་|ཆུང་ཀུ།"

#: ../mcs-plugin/xfwm4_plugin.c:1802
msgid "Distance|Wide"
msgstr "གྱང་ཚད་|རྒྱ་ཅན།"

#: ../mcs-plugin/xfwm4_plugin.c:1823
msgid "Wrap workspaces"
msgstr "ལཱ་གི་ས་སྒོ་ཚུའི་ལོག་མཚམས་བཟོ།"

#: ../mcs-plugin/xfwm4_plugin.c:1829
msgid "Wrap workspaces when the pointer reaches a screen edge"
msgstr "གསལ་གཞིའི་མཐའམ་ལུ་དཔག་བྱེད་ལྷོད་པའི་སྐབས་ལཱ་གི་ས་སྒོ་ཚུ་ལོག་མཚམས་བཟོ།"

#: ../mcs-plugin/xfwm4_plugin.c:1837
msgid "Wrap workspaces when dragging a window off the screen"
msgstr "སྒོ་སྒྲིག་ཅིག་གསལ་གཞི་གི་ཕྱི་ཁར་འདྲུད་པའི་སྐབས་ལཱ་གི་ས་སྒོ་ཚུ་ལོག་མཚམས་བཟོ།"

#: ../mcs-plugin/xfwm4_plugin.c:1847
msgid "Edge Resistance :"
msgstr "བཀག་ནུས་ཀྱི་མཐའམ་:"

#: ../mcs-plugin/xfwm4_plugin.c:1854
msgid "Resistance|Small"
msgstr "བཀག་ནུས་|ཆུང་ཀུ།"

#: ../mcs-plugin/xfwm4_plugin.c:1861
msgid "Resistance|Wide"
msgstr "བཀག་ནུས་|རྒྱ་ཅན།"

#: ../mcs-plugin/xfwm4_plugin.c:1883
msgid "Opaque move and resize"
msgstr "དྭངས་སྒྲིབ་ཅན་སྤོ་བཤུད་དང་ཚད་བསྐྱར་བཟོ།"

#: ../mcs-plugin/xfwm4_plugin.c:1888
msgid "Display content of windows when resizing"
msgstr "ཚད་བསྐྱར་བཟོ་འབད་བའི་སྐབས་ཝིན་ཌོསི་གི་ནང་དོན་བཀྲམ་སྟོན་འབད།"

#: ../mcs-plugin/xfwm4_plugin.c:1894
msgid "Display content of windows when moving"
msgstr "སྤོ་བཤུད་འབད་བའི་སྐབས་ཝིན་ཌོསི་གི་ནང་དོན་བཀྲམ་སྟོན་འབད།"

#: ../mcs-plugin/xfwm4_plugin.c:1899
msgid "Double click action"
msgstr "ཐེངས་གཉིས་ཨེབ་གཏང་བྱ་བ།"

#: ../mcs-plugin/xfwm4_plugin.c:1901
msgid "Action to perform when double clicking on title bar :"
msgstr "མགོ་མིང་ཕྲ་རིང་གུ་ཨེབ་གཏང་པའི་སྐབས་ལཱ་འགན་གྲུབ་ནི་ལུ་བྱ་བ་:"

#: ../mcs-plugin/xfwm4_plugin.c:1906
msgid "Advanced"
msgstr "མཐོ་རིམ།"

#. the button label in the xfce-mcs-manager dialog
#: ../mcs-plugin/xfwm4_plugin.c:2051
msgid "Button Label|Window Manager"
msgstr "ཨེབ་རྟའི་ཁ་ཡིག་|སྒོ་སྒྲིག་འཛིན་སྐྱོང་པ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:53
msgid "Do you really want to remove this keybinding theme ?"
msgstr "ཁྱོད་ཀྱིས་ཀི་བཱའིན་ཌིང་བརྗོད་དོན་འདི་ཐད་རི་འབའ་རི་རྩ་བསྐྲད་གཏང་ནི་ཨིན་ན?"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:134
#: ../mcs-plugin/xfwm4_shortcuteditor.c:140
msgid "Add keybinding theme"
msgstr "ཀི་བཱའིན་ཌིང་བརྗོད་དོན་ཁ་སྐོང་རྐྱབས།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:144
msgid "Enter a name for the theme:"
msgstr "བརྗོད་དོན་གྱི་དོན་ལས་མིང་བཙུགས་:"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:180
msgid "A keybinding theme with the same name already exists"
msgstr "མིང་གཅིག་པ་ཡོད་མི་ཀི་བཱའིན་ཌིང་བརྗོད་དོན་ཧེ་མ་ལས་ཡོད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:186
msgid "You have to provide a name for the keybinding theme"
msgstr "ཁྱོད་ཀྱིས་ཀི་བཱའིན་ཌིང་བརྗོད་དོན་གྱི་དོན་ལས་མིང་བྱིན་དགོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:363
msgid "Close window"
msgstr "སྒོ་སྒྲིག་ཁ་བསྡམས།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:365
msgid "Maximize window vertically"
msgstr "སྒོ་སྒྲིག་ཀེར་ཕྲང་སྦེ་སྦོམ་བཟོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:366
msgid "Maximize window horizontally"
msgstr "སྒོ་སྒྲིག་ཐད་སྙོམས་སྦེ་སྦོམ་བཟོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:367
#, fuzzy
msgid "Fill window horizontally"
msgstr "སྒོ་སྒྲིག་ཐད་སྙོམས་སྦེ་སྦོམ་བཟོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:368
#, fuzzy
msgid "Fill window vertically"
msgstr "སྒོ་སྒྲིག་ཀེར་ཕྲང་སྦེ་སྦོམ་བཟོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:372
msgid "Stick window"
msgstr "སྒོ་སྒྲིག་སྦྱཡ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:373
msgid "Cycle windows"
msgstr "བསྐྱར་འཁོར་ཝིན་ཌོསི།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:374
msgid "Move window up"
msgstr "སྒོ་སྒྲིག་ཡར་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:375
msgid "Move window down"
msgstr "སྒོ་སྒྲིག་མར་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:376
msgid "Move window left"
msgstr "སྒོ་སྒྲིག་གཡོན་ལུ་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:377
msgid "Move window right"
msgstr "སྒོ་སྒྲིག་གཡས་ལུ་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:378
msgid "Resize window up"
msgstr "སྒོ་སྒྲིག་ཡར་ཚད་བསྐྱར་བཟོ་འབད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:379
msgid "Resize window down"
msgstr "སྒོ་སྒྲིག་མར་ཚད་བསྐྱར་བཟོ་འབད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:380
msgid "Resize window left"
msgstr "སྒོ་སྒྲིག་གཡོན་ལུ་ཚད་བསྐྱར་བཟོ་འབད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:381
msgid "Resize window right"
msgstr "སྒོ་སྒྲིག་གཡས་ལུ་ཚད་བསྐྱར་བཟོ་འབད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:382
msgid "Cancel move/resize window"
msgstr "སྤོ་བཤུད་ཆ་མེད་གཏང་/སྒོ་སྒྲིག་ཚད་བསྐྱར་བཟོ་འབད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:383
msgid "Raise window"
msgstr "སྒོ་སྒྲིག་ཆེར་བསྐྱེད་འབད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:384
msgid "Lower window"
msgstr "འོག་གི་སྒོ་སྒྲིག།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:385
msgid "Toggle above"
msgstr ""

#: ../mcs-plugin/xfwm4_shortcuteditor.c:386
msgid "Toggle fullscreen"
msgstr "གསལ་གཞི་གངམ་སོར་སྟོན་འབད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:387
msgid "Upper workspace"
msgstr "ཡར་གྱི་ལཱ་གི་ས་སྒོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:388
msgid "Bottom workspace"
msgstr "གཤམ་གི་ལཱ་གི་ས་སྒོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:389
msgid "Left workspace"
msgstr "གཡོན་གྱི་ལཱ་གི་ས་སྒོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:390
msgid "Right workspace"
msgstr "གཡས་ཀྱི་ལཱ་གི་ས་སྒོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:391
msgid "Next workspace"
msgstr "ཤུལ་མམ་གྱི་ལཱ་གི་ས་སྒོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:392
msgid "Previous workspace"
msgstr "ཧེ་མའི་ལཱ་གི་ས་སྒོ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:393
msgid "Add workspace"
msgstr "ལཱ་གི་ས་སྒོ་ཁ་སྐོང་རྐྱབས།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:394
msgid "Delete workspace"
msgstr "ལཱ་གི་ས་སྒོ་བཏོན་གཏང་།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:395
msgid "Move window to next workspace"
msgstr "ཤུལ་མམ་གྱི་ལཱ་གི་ས་སྒོ་ལུ་སྒོ་སྒྲིག་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:396
msgid "Move window to previous workspace"
msgstr "ཧེ་མའི་ལཱ་གི་ས་སྒོ་ལུ་སྒོ་སྒྲིག་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:397
msgid "Move window to upper workspace"
msgstr "མགོ་གི་ལཱ་གི་ས་སྒོ་ལུ་སྒོ་སྒྲིག་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:398
msgid "Move window to bottom workspace"
msgstr "གཤམ་གྱི་ལཱ་གི་ས་སྒོ་ལུ་སྒོ་སྒྲིག་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:399
msgid "Move window to left workspace"
msgstr "གཡོན་གྱི་ལཱ་གི་ས་སྒོ་ལུ་སྒོ་སྒྲིག་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:400
msgid "Move window to right workspace"
msgstr "གཡས་ཀྱི་ལཱ་གི་ས་སྒོ་ལུ་སྒོ་སྒྲིག་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:401
msgid "Show desktop"
msgstr "ཌེཀསི་ཊོཔ་སྟོན།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:402
msgid "Cancel window action"
msgstr "སྒོ་སྒྲིག་གི་བྱ་བ་ཆ་མེད་གཏང་།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:403
msgid "Window operations menu"
msgstr "སྒོ་སྒྲིག་བཀོལ་སྤྱོད་ཚུའི་དཀར་ཆག།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:511
#, fuzzy, c-format
msgid "Move window to workspace %d"
msgstr "ལཱ་གི་ས་སྒོ་ %d ལུ་སྒོ་སྒྲིག་བཤུད།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:591
msgid "Cannot open the theme directory !"
msgstr "བརྗོད་དོན་སྣོད་ཐོ་ཁ་ཕྱེ་མི་ཚུགས་!"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:610
#, c-format
msgid ""
"Cannot open %s : \n"
"%s"
msgstr ""
"%s ཁ་ཕྱེ་མི་ཚུགས་: \n"
"%s"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:624
#: ../mcs-plugin/xfwm4_shortcuteditor.c:635
#, c-format
msgid ""
"Cannot write in %s : \n"
"%s"
msgstr ""
"%s ནང་འབྲི་མི་ཚུགས་: \n"
"%s"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:794
msgid ""
"Shortcut already in use !\n"
"Are you sure you want to use it ?"
msgstr ""
"མགྱོགས་ཐབས་ཧེ་མ་ལས་ལག་ལེན་འཐབ་ཡོད་!\n"
"ཁྱོད་ཀྱིས་དེ་ལག་ལེན་འཐབ་ནི་ལུ་ངེས་གཏན་ཨིན་ན ?"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:858
msgid "Compose shortcut for :"
msgstr "གི་དོན་ལས་མགྱོགས་ཐབས་བརྩམ་:"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:862
msgid "Compose shortcut"
msgstr "མགྱོགས་ཐབས་བརྩམ།"

#: ../mcs-plugin/xfwm4_shortcuteditor.c:864
msgid "Cancel"
msgstr ""

#: ../mcs-plugin/xfwm4_shortcuteditor.c:868
msgid "No shortcut"
msgstr "མགྱོགས་ཐབས་མེད།"

#. TRANSLATORS: "(on %s)" is like "running on" the name of the other host
#: ../src/hints.c:84
#, c-format
msgid "%s (on %s)"
msgstr "%s (གུ་ %s)"

#: ../src/main.c:135
#, c-format
msgid "%s: Segmentation fault"
msgstr "%s: ཆ་བགོས་ཀྱི་སྐྱོན།"

#: ../src/menu.c:40
msgid "Ma_ximize"
msgstr "སྦོམ་བཟོ། (_x)"

#: ../src/menu.c:41
msgid "Un_maximize"
msgstr "སྦོམ་བཟོ་བཤོལ། (_m)"

#: ../src/menu.c:42
msgid "_Hide"
msgstr "སྦ་བཞག། (_H)"

#: ../src/menu.c:43
msgid "Hide _all others"
msgstr "གཞན་ཚུ་ཆ་མཉམ་སྦ་བཞག། (_a)"

#: ../src/menu.c:44
msgid "S_how"
msgstr "སྟོན། (_h)"

#: ../src/menu.c:45
msgid "_Shade"
msgstr "ནག་གྲིབ། (_S)"

#: ../src/menu.c:46
msgid "Un_shade"
msgstr "ནག་གྲིབ་བཤོལ། (_s)"

#: ../src/menu.c:47
msgid "S_tick"
msgstr "སྦྱར། (_t)"

#: ../src/menu.c:48
msgid "Uns_tick"
msgstr "སྦྱར་བཤོལ། (_t)"

#: ../src/menu.c:49
msgid "Context _help"
msgstr "སྐབས་དོན་གྲོགས་རམ། (_h)"

#: ../src/menu.c:50 ../src/menu.c:51
msgid "Always on top"
msgstr "ཨ་རྟག་རང་མགོ་ལུ།"

#: ../src/menu.c:52
msgid "Send to..."
msgstr "ལུ་གཏང་..."

#: ../src/menu.c:54
msgid "_Close"
msgstr "ཁ་བསྡམས། (_C)"

#: ../src/menu.c:57
msgid "Destroy"
msgstr "རྩ་མེད་གཏང་།"

#: ../src/menu.c:60
msgid "_Quit"
msgstr "སྤང་། (_Q)"

#: ../src/menu.c:61
msgid "Restart"
msgstr "ལོག་འགོ་བཙུགས།"

#: ../src/menu.c:169
#, c-format
msgid "Workspace %i (%s)"
msgstr "ལཱ་གི་ས་སྒོ་ %i (%s)"

#: ../src/menu.c:173
#, c-format
msgid "Workspace %i"
msgstr "ལཱ་གི་ས་སྒོ་ %i "

#: ../src/menu.c:409
#, c-format
msgid "%s: GtkMenu failed to grab the pointer\n"
msgstr "%s: ཇི་ཊི་ཀེ་དཀར་ཆག་འདི་དཔག་བྱེད་འཛིན་ནི་ལུ་འཐུས་ཤོར་བྱུང་ཡོདཔ།\n"

#: ../src/settings.c:968
#, c-format
msgid "%s: Cannot allocate color %s\n"
msgstr "%s: ཚོས་གཞི་ %s སྤྲོད་མི་ཚུགས།\n"

#: ../src/settings.c:974
#, c-format
msgid "%s: Cannot parse color %s\n"
msgstr "%s: ཚོས་གཞི་ %s མིང་དཔྱད་འབད་མི་ཚུགས།\n"

#: ../mcs-plugin/xfce-wm-settings.desktop.in.h:1
msgid "Window Manager Settings"
msgstr "སྒོ་སྒྲིག་འཛིན་སྐྱོང་པ་སྒྲིག་སྟངས་ཚུ།"

#: ../mcs-plugin/xfce-wm-settings.desktop.in.h:2
msgid "Xfce 4 Window Manager Settings"
msgstr "ཨེཀསི་ཨེཕ་སི་ཨི་༤སྒོ་སྒྲིག་འཛིན་སྐྱོང་པ་སྒྲིག་སྟངས་ཚུ།"

#: ../mcs-plugin/xfce-wmtweaks-settings.desktop.in.h:1
msgid "Advanced Configuration"
msgstr "མཐོ་རིམ་རིམ་སྒྲིག།"

#: ../mcs-plugin/xfce-wmtweaks-settings.desktop.in.h:3
msgid "Xfce 4 Window Manager Tweaks"
msgstr "ཨེཀསི་ཨེཕ་སི་ཨི་༤སྒོ་སྒྲིག་འཛིན་སྐྱོང་པ་གཅུཝ་ཨིན།"

#: ../mcs-plugin/xfce-workspaces-settings.desktop.in.h:1
msgid "Workspaces Settings"
msgstr "ལཱ་གི་ས་སྒོ་ཚུའི་སྒྲིག་སྟངས་ཚུ།"

#: ../mcs-plugin/xfce-workspaces-settings.desktop.in.h:2
msgid "Xfce 4 Workspaces Settings"
msgstr "ཨེཀསི་ཨེཕ་སི་ཨི་༤ལཱ་གི་ས་སྒོ་ཚུའི་སྒྲིག་སྟངས་ཚུ།"

#, fuzzy
#~ msgid "Workspace %02d"
#~ msgstr "ལཱ་གི་ས་སྒོ་ %d"
